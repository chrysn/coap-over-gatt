# CoAP over GATT (Bluetooth Low Energy Generic Attributes)

This is the working area for the individual Internet-Draft, "CoAP over GATT (Bluetooth Low Energy Generic Attributes)".

* [Editor's Copy](https://gitlab.com/chrysn/coap-over-gatt/)
* [Datatracker Page](https://datatracker.ietf.org/doc/draft-amsuess-core-coap-over-gatt)
* [Individual Draft](https://datatracker.ietf.org/doc/html/draft-amsuess-core-coap-over-gatt)


## Contributing

See the
[guidelines for contributions](https://gitlab.com/chrysn/coap-over-gatt/-/blob/master/CONTRIBUTING.md).

Contributions can be made by creating pull requests.
The GitLab interface supports creating pull requests using the Edit (✏) button.


## Command Line Usage

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

Command line usage requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/main/doc/SETUP.md).

